let SPEED = 7;

let lib = undefined;

function getCollectible(x, y) {
    return new Rectangle(x, y, 25, 25, '#673AB7');
}

function getPlatform(x, y) {
    return new Rectangle(x, y, 200, 40, '#827717');
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function gameLoop() {
    // Handle input game.controller
    if (game.controller.left) {
        player.velocityX = -15;
    }
    if (game.controller.right) {
        player.velocityX = 15;
    }
    if (game.controller.up) {
        // Jump
        if (player.jumpCounter < 2) {
            player.velocityY = -30;
            player.jumpCounter++;
        }
        // Reset UP command
        game.controller.up = false;
    }

    // Move player downwards
    player.rectangle.y += SPEED;

    // Check if player is on a platform
    let onGround = player.isOnGround();

    // Check for jump reset
    if (onGround && player.velocityY == 0) {
        player.jumpCounter = 0;
    }

    // Move in sidewards
    if (player.velocityX != 0) {
        player.velocityX -= (player.velocityX / Math.abs(player.velocityX));
    }
    player.setX(player.rectangle.x += player.velocityX);

    // Stop falling when on ground
    if (onGround && player.velocityY >= 0) {
        player.velocityY = 0;
    } else {
        player.velocityY += player.gravity;
    }

    if (player.velocityY > 0) {
        // Fall down in small steps to prevent sticking in platforms
        for (let i = 0; i < player.velocityY; i++) {
            onGround = player.isOnGround();
            if (!onGround) {
                player.rectangle.y += Math.sign(player.velocityY);
            }
        }
    } else {
        player.rectangle.y += player.velocityY;
    }

    // Check for player death
    if (player.rectangle.y > game.canvas.height) {
        game.loose();
    }

    // Move platforms downwards
    // Array platformsToSplice is needed because of
    // splicing directly in the loop will lead to ignoring the platform afterwards
    let platformsToSplice = [];
    game.platforms.forEach(function (platform, index) {
        platform.y += SPEED;
        if (platform.y > game.canvas.height) {
            platformsToSplice.push(index);
        }
    });
    platformsToSplice.forEach(function(index){
        game.platforms.splice(index, 1);
    });

    // Move collectibles downwards
    game.collectibles.forEach(function (collectible, index) {
        collectible.y += SPEED;
        if (collectible.y > game.canvas.height) {
            game.collectibles.splice(index, 1);
        }
    });

    // Check for collectible collisions
    game.collectibles.forEach(function (collectible, index) {
        if (collectible.doesCollide(player.rectangle)) {
            game.increaseScore();
            game.collectibles.splice(index, 1);
        }
    });

    // Spawn next platform
    if (game.platforms[game.platforms.length - 1].y >= game.nextPlatformGap) {
        let newRectangle = getPlatform(getRandom(0, game.canvas.width - 100), game.platforms[game.platforms.length - 1].y - game.nextPlatformGap);
        game.platforms.push(newRectangle);
        game.nextPlatformGap = getRandom(100, 500);
        if (getRandom(1, 3) == 1) {
            game.collectibles.push(getCollectible(newRectangle.x + newRectangle.width / 2 - 15, newRectangle.y - 55));
        }
    }

    game.draw();
    player.draw();
}

let game = new function () {

    this.canvas = {
        width: 1000,
        height: 1500,
        color: '#212121'
    };
    this.controller = {
        left: false,
        right: false,
        up: false
    };
    this.platforms = [
        new Rectangle(0, 350, this.canvas.width, 100, '#827717')
    ];
    this.collectibles = [];
    this.score = 0;
    this.nextPlatformGap = 200;

    this.increaseScore = function () {
        this.score++;
        document.getElementById('score').innerHTML = 'Score: ' + this.score;
    };

    this.loose = function(){
        document.body.style.backgroundColor = this.canvas.color;
        document.getElementById('score').innerHTML = '&nbsp;';
        document.title = '[lost] ' + document.title;

        lib.setGameLoop(function(){
            game.draw();
            lib.drawText('You lost. Score: ' + game.score, game.canvas.width/2, game.canvas.height/2, 'red', 90, 'center');
            lib.drawText('(press any key to continue)', game.canvas.width/2, game.canvas.height/2 + 50, 'red', 50, 'center');
        });

        lib.onKeyDown(function () {
            location.reload();
        });
    };

    this.draw = function () {
        lib.setSize(this.canvas.width, this.canvas.height, true, 0.0, 0.1);
        lib.setBackground(this.canvas.color);

        this.platforms.forEach(function (platform) {
            lib.drawRectangleObject(platform);
        });

        this.collectibles.forEach(function (collectible) {
            lib.drawRectangleObject(collectible);
        });
    }
};

let player = new function () {
    this.rectangle = new Rectangle(100, 100, 100, 100, '#673AB7');

    this.gravity = 1;
    this.velocityX = 0;
    this.velocityY = 0;

    this.jumpCounter = 0;

    this.setX = function (x) {
        if (x < 0) {
            this.rectangle.x = game.canvas.width + x % game.canvas.width;
        } else {
            this.rectangle.x = x % game.canvas.width;
        }
    };

    this.isOnGround = function () {
        let onGround = false;
        game.platforms.forEach(function (platform) {
            if (player.rectangle.isAbove(platform)) {
                onGround = true;
            }
        });
        return onGround;
    };

    this.draw = function () {
        lib.drawRectangleObject(this.rectangle);
    }
};

window.onload = function () {
    // Set page design
    document.body.style.backgroundColor = '#424242';
    document.getElementById('score').style.color = '#827717';

    lib = new GameLib('canvas');
    lib.setGameLoop(gameLoop, 60);
    lib.onKeyDown(function (event) {
        switch (event.key) {
            case "ArrowLeft":
            case 'a':
                game.controller.left = true;
                return true;
            case "ArrowRight":
            case 'd':
                game.controller.right = true;
                return true;
            case " ":
            case "ArrowUp":
            case 'w':
                game.controller.up = true;
                return true;
            default:
                return false;
        }
    });
    lib.onKeyUp(function (event) {
        switch (event.key) {
            case "ArrowLeft":
            case 'a':
                game.controller.left = false;
                return true;
            case "ArrowRight":
            case 'd':
                game.controller.right = false;
                return true;
            case " ":
            case "ArrowUp":
            case 'w':
                game.controller.up = false;
                return true;
            default:
                return false;
        }
    });
};
